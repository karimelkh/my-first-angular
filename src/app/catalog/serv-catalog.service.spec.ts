import { TestBed } from '@angular/core/testing';

import { ServCatalogService } from './serv-catalog.service';

describe('ServCatalogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServCatalogService = TestBed.get(ServCatalogService);
    expect(service).toBeTruthy();
  });
});
