import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, pipe } from "rxjs";
import { CardsInterface } from "./interface/cards.interface";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ServCatalogService {
  constructor(private httpClient: HttpClient) {}

  get endpoint() {
    return "https://omgvamp-hearthstone-v1.p.rapidapi.com/";
  }

  // tslint:disable-next-line: ban-types
  getcards(setName: String, className?: String) {
    return this.set(setName);
  }

  getCardById(id: string) {
    return this.httpClient
      .get(`${this.endpoint}cards/${id}`, this.httpOptions)
      .pipe(map((card: CardsInterface[]) => card[0]));
  }

  search(name): Observable<CardsInterface[]> {
    return this.httpClient
      .get(`${this.endpoint}cards/search/${name}`, this.httpOptions)
      .pipe(map((cards: CardsInterface[]) => cards));
  }

  info(): Observable<{ classes: string[], sets: string[] }> {
    return this.httpClient.get(
      `${this.endpoint}info`,
      this.httpOptions).pipe(
      map((cards: { classes: string[], sets: string[] }) => cards)
    );
  }



  get endPoint() {
    return "https://omgvamp-hearthstone-v1.p.rapidapi.com/";
  }
  get httpOptions(): { headers: HttpHeaders } {
    let headers = new HttpHeaders();
    headers = headers.append(
      "x-rapidapi-host",
      "omgvamp-hearthstone-v1.p.rapidapi.com"
    );
    headers = headers.append(
      "x-rapidapi-key",
      "d99e92cde6mshffcf37544a876bep14eb15jsn1c1928e0dec3"
    );
    return {
      headers
    };
  }
  // tslint:disable-next-line: ban-types
  private set(name): Observable<CardsInterface[]> {
    return this.httpClient
      .get(`${this.endPoint}cards/sets/${name}`, this.httpOptions)
      .pipe(map((cards: CardsInterface[]) => cards));
  }
  // tslint:disable-next-line: ban-types
  private class(name: String) {
    return this.httpClient.get(
      `${this.endPoint}cards/classes/${name}`,
      this.httpOptions
    );
  }
}
