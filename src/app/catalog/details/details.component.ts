import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServCatalogService } from '../serv-catalog.service';
import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { CardsInterface } from '../interface/cards.interface';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  card$: Observable<CardsInterface>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private servCatalogService: ServCatalogService,
    @Inject(DOCUMENT) private document,
  ) {}

  ngOnInit() {
    this.card$ = this.activatedRoute.params.pipe(
      map(params => params.id),
      switchMap(id => this.servCatalogService.getCardById(id)),
      tap(card => console.log(card)),
      catchError( reason => {
        console.log(reason);
        return this.router.navigate(['..']);
      })
    );
  }
  loadImage(card: CardsInterface): Observable<CardsInterface> {
    console.log(document, card);
    if (card) {
      if (!card.img) {
        card.img = 'http://wow.zamimg.com/images/hearthstone/backs/animated/Card_Back_Troll.gif';
      }
    }
    const subject = new Subject<CardsInterface>();
    const img: HTMLImageElement = this.document.createElement('img');
    img.onload = () => {
      subject.next(card);
    };
    img.onerror = () => {
      card.img = 'http://wow.zamimg.com/images/hearthstone/backs/animated/Card_Back_Troll.gif';
      subject.next(card);
    };
    img.src = card.img;
    return subject.pipe();
  }

}

