import { Component, OnInit } from '@angular/core';
import { ServCatalogService } from '../serv-catalog.service';
import { Observable, Subject, BehaviorSubject, merge } from 'rxjs';
import { CardsInterface } from '../interface/cards.interface';
import { NgForm, Validators, FormGroup, FormControl } from '@angular/forms';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  cards$: Observable<CardsInterface[]>;
  info$: Observable<{ classes: string[], sets: string[] }>;
  query = 'default';
  filterForm$: BehaviorSubject<{set: string, classe: string}>;


   searchForm$: Subject<string>;


   filterForm = new FormGroup({
     set : new FormControl ('Basic', [ Validators.required]),
     classe : new FormControl('Druid', [ Validators.required]),

   });

  constructor(private servCatalogService: ServCatalogService ) {
  }

  ngOnInit() {

    this.searchForm$ = new Subject();
    this.filterForm$ = new BehaviorSubject({
      set : this.filterForm.get('set').value,
      classe :  this.filterForm.get('classe').value
    });
    this.cards$ = merge(
      this.searchForm$.pipe(
        switchMap(query => {
          return this.servCatalogService.search(query);
        })
      ),
      this.filterForm$.pipe(
        switchMap(query => {
          return this.servCatalogService.getcards(query.set, query.classe);
        })
      )
    );
    this.info$ = this.servCatalogService.info();
  }


  submit(monForm: NgForm) {
    console.log('SUBMIT', monForm, this.query);
    this.servCatalogService.info().subscribe(t => console.log(t));
    this.servCatalogService.search(this.query).subscribe(t => console.log(t));
    if (monForm.valid) {
      this.cards$ = this.servCatalogService.search(this.query);
    }

  }
}

