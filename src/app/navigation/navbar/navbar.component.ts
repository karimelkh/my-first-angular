import { Component, OnInit, Input } from '@angular/core';
import { NavbarDisplayService } from '../service/navbar-display.service';
import { BehaviorSubject, Observable } from 'rxjs';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  [x: string]: any;
  isCollapsed = true;
  title: string;
  logo: string;
  @Input()
  navbarDisplay: boolean;
  constructor(private navbarDisplayService:NavbarDisplayService) {
    this.logo = 'assets/img/Unknown.jpg';
    this.title = 'coucou';
  }
  ngOnInit() {
    this.navbarDisplayService.onChangeDisplay().subscribe((isDisplay: boolean) => (this.navbarDisplay = isDisplay));
  }

}
