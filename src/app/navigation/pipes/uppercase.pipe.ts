import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uppercase',
  pure: true
})
export class UppercasePipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    console.log(value);
    return value.toUpperCase();
  }

}

