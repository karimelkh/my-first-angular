import { Directive, HostListener , HostBinding} from '@angular/core';

@Directive({
  selector: '[appDirectiveOne]'
})
export class DirectiveOneDirective {

  @HostBinding('class.show') show: boolean;

  @HostListener('window:scroll', ['$event'])
onWindowScroll(event: any) {
  console.log('scrolling...', event);
}
  constructor() {

  }

}

